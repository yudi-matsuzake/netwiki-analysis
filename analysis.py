import numpy as np
import networkx as nx
import networkx.algorithms.community as nxc

import wikinx
import matplotlib.pyplot as plt
import sys
import random

import cdlib as cd
import cdlib.algorithms

import argparse

import pathlib

outdir = None
outfile = None

def mkdirs(path):
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)

def get_outpath(f):
    assert outdir is not None
    p = outdir / f
    mkdirs(p.parent)
    return p

def info(*args, **kwargs):
    assert outfile is not None
    with open(outfile, 'a') as f:
        print(*args, **kwargs, file = f)

def make_array_from_generator(generator, n, dtype = float):
    a = np.empty(n, dtype)
    for i, v in enumerate(generator):
        a[i] = v
    return a

def compute_degree_distribution(degrees):
    minimum, maximum = int(min(degrees)), int(max(degrees))
    dist = np.zeros(maximum - minimum + 1)

    dsum = 0
    for d in degrees:
        dsum += d
        dist[int(d) - minimum] += 1

    norm_factor = 1./dsum
    return np.arange(minimum, maximum + 1), norm_factor*dist

def compute_in_degree_distribution(G):
    degrees = [ G.in_degree[v] for v in G.nodes() ]
    return compute_degree_distribution(degrees)

def compute_out_degree_distribution(G):
    degrees = [ G.out_degree[v] for v in G.nodes() ]
    return compute_degree_distribution(degrees)

def get_in_degrees(G):
    n = G.number_of_nodes()
    return make_array_from_generator((G.in_degree[v] for v in G.nodes()), n)

def get_out_degrees(G):
    n = G.number_of_nodes()
    return make_array_from_generator((G.out_degree[v] for v in G.nodes()), n)

def plot_degree_distribution_log_log(ax, title, x, y):
    ax.set(
        title  = title,
        xlabel = '$k$',
        ylabel = '$p_k$',
        xscale = 'log',
        yscale = 'log'
    )
    ax.plot(x, y, marker = '.', linestyle = '')

def find_index_where(collection, f):
    for i, c in enumerate(collection):
        if f(c):
            return i
    return len(collection)

def min_bigger_than(collection, x):
    min = None

    for c in collection:
        if min is None or (c > x and x < min):
            min = c

    return min

def step(x, edge):
    return x if x > edge else edge

def plot_powerlaw(ax, label, x, y, gamma):
    minimum, maximum = np.min(y), np.max(y)

    nonzero_min = min_bigger_than(y, 0)

    i = find_index_where(x, lambda x : step(x, 1.0)**-gamma < maximum)
    j = len(x) - find_index_where(x[::-1], lambda x : step(x, 1.0)**-gamma >= nonzero_min) - 1

    ax.plot(x[i:j], x[i:j]**(-gamma), linestyle='--', label=label)

def plot_degree_distribution(G):
    in_degrees = get_in_degrees(G)
    out_degrees = get_in_degrees(G)

    in_x, in_y = compute_degree_distribution(in_degrees)
    out_x, out_y = compute_degree_distribution(out_degrees)

    fig, (in_ax, out_ax) = plt.subplots(1, 2, figsize = (7, 10))
    fig.tight_layout()
    plot_degree_distribution_log_log(in_ax, 'in-degree distribution', in_x, in_y)
    plot_degree_distribution_log_log(out_ax, 'out-degree distribution', out_x, out_y)

    gamma = 2.26
    plot_powerlaw(in_ax, f'$\gamma = {gamma}$', in_x, in_y, gamma)
    plot_powerlaw(out_ax, f'$\gamma = {gamma}$', out_x, out_y, gamma)
    in_ax.legend()
    out_ax.legend()
    plt.savefig(get_outpath('plots/degree-distribution.pdf'))

def approximate_cluster_coefficient(G, trials):
    count = 0.0

    for n in random.sample([ *G.nodes() ], trials):

        adj = [ *G.adj[n] ]
        if len(adj) >= 2:
            u, v = random.sample(adj, 2)

            if G.has_predecessor(u, v) or G.has_successor(u, v):
                count += 1

    return count / trials

def shortest_path_length(G, source, target):
    try:
        return nx.shortest_path_length(G, source, target), True
    except nx.NetworkXNoPath:
        return None, False

def pair_generator(G):
    for v in G.nodes():
        for u in G.nodes():
            yield (u, v)

def estimate_avg_shortest_path(G, trials):

    s = 0.0

    nodes = make_array_from_generator(
        (v for v in G.nodes()),
        G.number_of_nodes(),
        dtype = int
    )

    count = 0
    while count < trials:
        u = random.randint(0, G.number_of_nodes()-1)
        v = random.randint(0, G.number_of_nodes()-1)

        l, has_path = shortest_path_length(G, nodes[u], nodes[v])

        if has_path:
            s += l
            count += 1

    return s / trials

def build_degree_correlation_function(G):
    degrees = make_array_from_generator(
        (G.degree[v] for v in G.nodes()),
        G.number_of_nodes()
    )

    max_degree = int(max(degrees))
    min_degree = int(min(degrees))
    n_degree = max_degree - min_degree + 1

    d = np.zeros(n_degree)
    c = np.zeros(n_degree)

    for v in G.nodes():
        di = G.degree[v] - min_degree
        for i in G.neighbors(v):
            d[di] += G.degree[i]
            c[di] += 1

    c[c == 0] = 1
    return np.arange(min_degree, max_degree + 1), d/c

def plot_degree_correlation_function(G):
    x, y = build_degree_correlation_function(G)

    fig, ax = plt.subplots(1, 1)

    ax.set(
        title = 'degree correlation function',
        xlabel = '$k$',
        ylabel = '$k_{nn}(k)$'
    )

    ax.plot(x, y, 'o')
    ax.plot(x, np.poly1d(np.polyfit(x, y, 1))(x))

    plt.savefig(get_outpath('plots/degree-correlation-function.pdf'))

def take(collection, n):
    for i, c in enumerate(collection):
        if i < n:
            yield c
        else:
            break

def print_top_influencers(G, v, top, key):
    rank = sorted(v, key = key, reverse = True)
    for i, j in enumerate(take(rank, top)):
        info(f'{i + 1} - {G.nodes[j]["title"]}')

def connected_components(G):
    if G.number_of_nodes() == 0: return []

    to_visit = [ next(iter(G.nodes())) ]
    visited = set()


    def create_component_from(v):
        c = []

        to_visit = [ v ]

        while len(to_visit) > 0:
            v = to_visit.pop()

            if v not in visited:
                visited.add(v)
                c.append(v)

                for u in G.successors(v):
                    to_visit.append(u)
                for u in G.predecessors(v):
                    to_visit.append(u)
        return c

    components = []
    for v in G.nodes():
        if v not in visited:
            components.append(create_component_from(v))
            visited.add(v)

    return components
# nx.write_graphml(G, 'scale.graphml')

def directed_processing(args):
    G = wikinx.load_graph(args.nodes_csv, args.links_csv)

    # nx.write_gml(G, 'wiki.gml')

    info(f'number of nodes: {G.number_of_nodes()}')
    info(f'number of links: {G.number_of_edges()}')

    plot_degree_distribution(G)

    trials = int(G.number_of_nodes()*0.25)
    info(f'average clustering coefficient (approximation with {trials} trials): {approximate_cluster_coefficient(G, trials = trials)}')

    trials = G.number_of_nodes()*10
    info(f'average shortest path (aproximation with {trials} trials: {estimate_avg_shortest_path(G, trials)}')

    r = nx.degree_pearson_correlation_coefficient(G)
    info(f'pearson degree correlation coefficient = {r}')
    info(f'{ "assortative" if r < 0 else ("disassortative" if r > 0 else "neutral") }')

    plot_degree_correlation_function(G)

    C = connected_components(G)
    giant_component = max(C, key=len)
    info(f'number of connected components: {len(C)}')
    info(f'size of the giant component: {len(giant_component)}')
    # C = [ *nxc.greedy_modularity_communities(G) ]

def undirected_processing(args):
    G = wikinx.load_graph(args.nodes_csv, args.links_csv, as_digraph=False)

    info('input files: ', args.nodes_csv, args.links_csv)

    top = 15
    info(f'top {top} most influential artists:')

    node_degree = lambda v : G.degree[v]
    print_top_influencers(G, G.nodes(), top, key = node_degree)

    top_communities = 5
    top = 10

    node_clusters = cd.algorithms.louvain(G)
    C = node_clusters.communities
    info(f'number of communities = {len(C)}')

    for community_id, community in enumerate(C):
        for v in community:
            G.nodes[v]['community'] = community_id

    nx.write_gexf(G, get_outpath('wiki-communities.gexf'))

    communities_rank = sorted(C, key = len, reverse = True)

    for i, j in enumerate(take(communities_rank, top_communities)):
        info(f'{i + 1} - community with size {len(j)} ------------------')
        print_top_influencers(G, j, top, node_degree)

def main(args):
    directed_processing(args)
    undirected_processing(args)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='extracts information about the wiki network')

    parser.add_argument('prefix', help = 'folder to put the output data')
    parser.add_argument('nodes_csv', help = 'csv file with the nodes')
    parser.add_argument('links_csv', help = 'csv file with the links')

    args = parser.parse_args()

    mkdirs(args.prefix)

    outdir = pathlib.Path(args.prefix)

    outfile =  outdir / 'info.txt'
    open(outfile, 'w')

    main(args)

