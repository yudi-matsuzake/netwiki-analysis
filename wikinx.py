import pandas as pd
import networkx as nx

def load_graph(nodes_csv, links_csv, as_digraph = True):
    nodes_data = pd.read_csv(nodes_csv, sep='|')
    links_data = pd.read_csv(links_csv, sep='|')

    G = nx.DiGraph() if as_digraph is True else nx.Graph()

    G.add_nodes_from([
        (label, { 'title' : title }) \
            for label, title in zip(nodes_data['id'], nodes_data['label'])
    ])

    G.add_edges_from([
        (f, t) for f, t in zip(links_data['from'], links_data['to'])
    ])

    return G
